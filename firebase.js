import firebase from 'firebase';


const firebaseConfig = {
    apiKey: "AIzaSyBRrv4KyNz8qZ4e8kHsN_DISz4070KZ_kY",
    authDomain: "next-e-d3e8e.firebaseapp.com",
    projectId: "next-e-d3e8e",
    storageBucket: "next-e-d3e8e.appspot.com",
    messagingSenderId: "810062227907",
    appId: "1:810062227907:web:1cdd04447bfbf4ed2f7576"
};

const app = !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

const db = app.firestore();


export default db;