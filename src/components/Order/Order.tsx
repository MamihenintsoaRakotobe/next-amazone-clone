import React,{FC} from 'react'
import { IOrders } from '../../pages/orders';
import Currency from 'react-currency-formatter';
import moment from 'moment';

interface IOrder{
    order:IOrders
}

const Order:FC<IOrder> = ({order}) => {
    const {id,amount,amountShipping,items,timestamp,images} = order;
    return (
        <div className="relative border rounded-md">
            
            <div className="flex items-center space-x-10 p-5 bg-gray-300 text-sm text-gray-600">
                <div>
                    <p className="font-bold text-xs"> ORDER PLACED</p>
                    <p> {moment.unix(timestamp).format('DD MMM YYYY')} </p>
                </div>

                <div>
                    <p className="text-xs font-bold">TOTAL</p>
                    <p>
                        <Currency quantity={amount} currency="GBP"/>
                        {" "}- Next-Day Delivery {" "}
                        <Currency quantity={amountShipping} currency="GBP"/>
                    </p>
                </div>

                <p className="text-sm whitespace-nowrap sm:text-xl self-end flex-1 text-right text-pink-500">
                    {items.length}{" "} items
                </p>

                <p className="absolute top-2 right-2 w-40 lg:w-72 truncate text-xs whitespace-nowrap">
                    ORDER # {id}
                </p>
            </div>

            <div className="p-5 sm:p-10 bg-white">
                <div className="flex items-center space-x-6 overflow-x-auto">
                    {images.map((image,i) => (
                        <img loading="lazy" key={i} src={image} alt="" className="h-20 object-contain sm:h-32"/>
                    ))}
                </div>
            </div>

        </div>
    )
}

export default Order
