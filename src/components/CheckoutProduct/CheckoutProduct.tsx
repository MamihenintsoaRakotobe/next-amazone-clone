import React, {FC} from 'react';
import Image from 'next/image';
import { IFullProductProps } from '../Product/Product';
import { StarIcon } from '@heroicons/react/solid';
import Currency from 'react-currency-formatter';
import { addToBasket, removeFromBasket } from '../../slices/basketSlice';
import { useDispatch } from 'react-redux';

interface ICheckoutProduct{
    product:IFullProductProps,
}

const CheckoutProduct:FC<ICheckoutProduct> = ({product}) => {
    const dispatch = useDispatch();
    const {id,title,description,image,price,category,rating,hasPrime} = product;

    const addItemToBasket = () => {
        const addedProduct:IFullProductProps ={
            id,
            title,
            description,
            image,
            price,
            category,
            rating,
            hasPrime
        }

        //sending the product as an action to redux store
        dispatch(addToBasket(addedProduct));
    }

    const removeItemFromBasket  =() => {
        dispatch(removeFromBasket({id}));
    }
    return (
        <div className="grid grid-cols-5">
            <Image  src={image} width={200} height={200} objectFit="contain"/>
            {/** middle */}
            <div className="col-span-3 mx-5">
                <p>{title}</p>
                <div className="flex">
                    {Array(rating).fill(0).map((_,i) => (
                        <StarIcon key={i} className="h-5 text-pink-500"/>
                    ))}
                </div>
                <p className="text-xs my-2 line-clamp-3">{description}</p>
                <Currency quantity={price} currency="GBP"/>
                {hasPrime && (
                    <div className="flex items-center sapce-x-2">
                        <img loading={"lazy"} className="w-12" src="https://links.papareact.com/fdw" alt=""/>
                        <p className="text-xs text-gray-500"> FREE next-day delivery </p>
                    </div>
                )}
            </div>

            <div className="flex flex-col space-y-2 my-auto justify-self-end">
                <button onClick={addItemToBasket} className="button">
                    Add to basket
                </button>
                <button onClick={removeItemFromBasket} className="button">
                    Remove from basket
                </button>
            </div>
        </div>
    )
}

export default CheckoutProduct;
