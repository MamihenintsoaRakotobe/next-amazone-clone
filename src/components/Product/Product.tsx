import React, {FC, useState} from 'react';
import { IProducts } from '../../pages';
import Image from 'next/image';
import {StarIcon} from '@heroicons/react/solid';
import Currency from 'react-currency-formatter';
import { useDispatch } from 'react-redux';
import {addToBasket} from '../../slices/basketSlice';

interface IProduct{
    product:IProducts
}

export interface IFullProductProps{
    id:number;
    title:string;
    price:number;
    description:number;
    category:string;
    image:string;
    rating:number;
    hasPrime:boolean;
}

const Product:FC<IProduct> = ({product}) => {
    const dispatch = useDispatch();
    const {id,title,description,image,price,category} = product;
    const MAX_RATING = 5;
    const MIN_RATING = 1;
    const [rating] = useState<number>(
        Math.floor(Math.random() * (MAX_RATING - MIN_RATING +1)) +MIN_RATING
    );
    const [hasPrime] = useState<boolean>(Math.random() < 0.5);
    
    const addItemToBasket = () => {
        const addedProduct:IFullProductProps ={
            id,
            title,
            description,
            image,
            price,
            category,
            rating,
            hasPrime
        }

        //sending the product as an action to redux store
        dispatch(addToBasket(addedProduct));
    }
    return (
        <div className="relative flex flex-col m-5 bg-white z-30 p-10">
            <p className="absolute top-2 right-2 text-sx italic text-gray-400">{category}</p>

            <Image src={image} width={200} height={200} objectFit="contain" />

            <h4 className="my-3">{title}</h4>

            <div className="flex">
                {Array<number>(rating).fill(2).map((_,i) => 
                    <StarIcon key={i} className="h-5 text-pink-500"/>
                )}
            </div>

            <p className="text-xs my-2 line-clamp-2">{description}</p>

            <div className="mb-5">
                <Currency quantity={price} currency="GBP"/>
            </div>

            {hasPrime && (
                <div className="flex items-center space-x-2 -mt-5">
                    <img className="w-12" src="https://links.papareact.com/fdw" alt=""/>
                    <p className="text-xs text-gray-500"> FREE next-day delivery </p>
                </div>
                
            )}
            <button onClick={addItemToBasket} className="mt-auto button">
                Add to basket
            </button>
        </div>
    )
}

export default Product;
