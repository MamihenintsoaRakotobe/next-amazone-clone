import { getSession } from 'next-auth/client';
import Head from 'next/head';
import Baner from '../components/Baner/Baner';
import Header from '../components/Header/Header';
import ProductFeed from '../components/ProductFeed/ProductFeed';


export interface IProducts {
  id:number;
  title:string;
  price:number;
  description:number;
  category:string;
  image:string;
}

export default function Home({products}:{products:IProducts[]}) {
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      

      <Header/>

      <main className="max-w-screen-2xl mx-auto">
        {/** baner */}
        <Baner />
        {/** fakestorapi feed */}
        <ProductFeed products={products}/>
      </main>
    </div>
  )
}


export const getServerSideProps = async(context:any) => {
  const products:IProducts[] = await fetch("https://fakestoreapi.com/products").then((res) => res.json());
  const session = await getSession(context);
  return {
    props:{
      products,
      session
    }
  }
}