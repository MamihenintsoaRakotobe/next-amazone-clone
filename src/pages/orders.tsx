import React from 'react';
import Header from '../components/Header/Header';
import { getSession, useSession } from 'next-auth/client';
import db from '../../firebase';
import moment from 'moment';
import Order from '../components/Order/Order';

export interface IOrders{
    id:string;
    amount:number;
    amountShipping:number;
    images:string[];
    timestamp:number;
    items:{
        amount_subtotal: number;
        amount_total: number;
        currency: string;
        description: string;
        id: string;
        object: string;
        quantity: number;
        price: {
            unit_amount: number;
            unit_amount_decimal: string;
        }
    }[]
}

const orders = ({orders}:{orders:IOrders[]}) => {
    const [session] = useSession();
    return (
        <div>
            <Header />
            <main className="max-w-screen-lg p-10 mx-auto">

                <h1 className="text-3xl mb-2 pb-1 border-b border-pink-500"> Your orders </h1>

                {session ?(
                    <h2> {orders.length} orders </h2>
                ):(
                    <h2> Please signin to see your orders</h2>
                )}

                <div className="mt-5 space-y-4">
                    {orders?.map((order:IOrders) => (
                        <Order key={order.id} order={order} />
                    ))}
                </div>
            </main>
        </div>
    )
}

export default orders;


export const getServerSideProps = async (context:any) => {
    const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
    const session = await getSession(context);
    if(!session) {
        return {
            props:{}
        }
    }

    // get the stripe order from firebase database
    const stripeOrders = await db
    .collection('users')
    //@ts-ignore
    .doc(session.user?.email)
    .collection('orders')
    .orderBy('timestamp','desc')
    .get();

    // stripe orders
    const orders:IOrders[] = await Promise.all(
        stripeOrders.docs.map(async(order) =>({
            id: order.id,
            amount: order.data().amount,
            amountShipping: order.data().amount_shipping,
            images: order.data().images,
            timestamp: moment(order.data().timestamp.toDate()).unix(),
            items:(
                await stripe.checkout.sessions.listLineItems(order.id,{
                    limit:100,
                })
            ).data,
        }))
    )

    return {
        props:{
            orders
        }
    }
}