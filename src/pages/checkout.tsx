import React from 'react';
import Header from '../components/Header/Header';
import Image from 'next/image';
import { useSelector } from 'react-redux';
import { selectItems, selectTotal } from '../slices/basketSlice';
import CheckoutProduct from '../components/CheckoutProduct/CheckoutProduct';
import { IFullProductProps } from '../components/Product/Product';
import Currency from 'react-currency-formatter';
import { useSession } from 'next-auth/client';
import { loadStripe } from '@stripe/stripe-js';
import axios from 'axios';

//@ts-ignore
const stripePromise = loadStripe(process.env.STRIPE_PUBLIC_KEY);

const checkout = () => {
    const items:IFullProductProps[] = useSelector(selectItems);
    const total = useSelector(selectTotal);
    const [session] = useSession();
    const createCheckoutSession = async() => {
        const stripe = await stripePromise;

        // call the backend api stripe payment
        const checkoutSession = await axios.post("/api/create-checkout-session",{
            items,
            email: session?.user?.email
        });
        // redirect to payment checkout
        const result = await stripe?.redirectToCheckout({
            sessionId: checkoutSession.data.id
        });
        if(result?.error){
            alert(result.error.message);
        }

    }
    return (
        <div className="bg-amazon_blue-lightgrey">
            <Header />
            <main className="lg:flex max-w-screen-2xl mx-auto">
                {/** Left */}
                <div className="flex-grow m-5 shadow-sm">

                    <Image src="https://links.papareact.com/ikj" width={1020} height={250} objectFit="contain" alt=""/>

                    <div className="flex flex-col p-5 space-y-10 bg-white">
                        <h1 className="text-3xl border-b pb-4">
                            {items.length === 0 ? 'Your shopping basket is empty' :'Shopping basket'}
                        </h1>

                        {items.map((item,i) =>(
                            <CheckoutProduct 
                                key={i} 
                                product={item}
                            />
                        ))}
                    </div>

                </div>

                {/**right */}
                <div className="flex flex-col bg-white p-10 shadow-md">
                    {items.length > 0 && (
                        <div>
                            <h2 className="whitespace-nowrap"> 
                                Subtotal({items.length} items) : {" "}
                                <span className="font-bold">
                                    <Currency quantity={total} currency="GBP" />
                                </span>
                            </h2>
                            <button
                                onClick={createCheckoutSession}
                                role="link"
                                disabled={!session}
                                className={`button mt-2 ${!session && 'from-gray-300 to gray-500 border-gray-200 text-gray-300 cursor-not-allowed'}`}
                            >
                                {!session ? 'Signin to checkout' : 'Proceed to checkout'}
                            </button>
                        </div>
                    )}
                </div>
            </main>
        </div>
    )
}

export default checkout;
