
import { createSlice } from "@reduxjs/toolkit";
import { IFullProductProps } from "../components/Product/Product";

interface IState {
  items:IFullProductProps[]
}

const initialState:IState = {
  items: [],
};

export const basketSlice = createSlice({
  name: "basket",
  initialState,
  reducers: {
    addToBasket: (state, action) => {
      state.items = [...state.items,action.payload];
    },
    removeFromBasket: (state, action) => {
      const index = state.items.findIndex(basketItem => basketItem.id === action.payload.id);
      let newBasket = [...state.items];
      if(index >=0){
        newBasket.splice(index,1);
      } else {

      }
      state.items = newBasket;
    },
  },
});

export const { addToBasket, removeFromBasket } = basketSlice.actions;

// Selectors - This is how we pull information from the Global store slice
export const selectItems = (state:any) => state.basket.items;
//@ts-ignore
export const selectTotal = (state:any) =>  state.basket.items.reduce((total,item) => total + item.price,0);

export default basketSlice.reducer;